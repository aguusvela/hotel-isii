/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotelisii;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import jxl.write.WriteException;
/**
 *
 * @author Mauro
 */
public class Sistema extends TimerTask{
    
    private final long millis=System.currentTimeMillis();  
    private java.sql.Date date; 
    private final String inputFile = "C:\\Users\\Hiplce\\Desktop\\hotelsito\\HotelISII1\\informedeldia_";
    private final String endString = ".xls";
    private InformeDiario inf = new InformeDiario(); //instancio objeto de clase que creara el informe en excell
    private ManagerHabitacion mh = new ManagerHabitacion();
    private ManagerReserva mr = new ManagerReserva();
    private ResultSet hab;
    private ResultSet res;
    
    //elimina la ultima coma del resultado de gethabocupadas
    public String method(String str) {
    if (str != null && str.length() > 0 && str.charAt(str.length() - 1) == ',') {
        str = str.substring(0, str.length() - 1);
    }
    return str;
}
    
    public String getHabOcupadas(){
        String resultado="";
        hab = mh.getHabitaciones();
        HIterator habis = new HIterator(hab);
        int cantH = mh.getRowCount();
        Object [] habs = (Object[]) habis.next();
        for(int e = 0; e < cantH; e++){
            String end = habs[4].toString();
            if((int)habs[0] == 1) {
                resultado = resultado.concat(end+",");
            }
            habs = (Object []) habis.next();
        }
        /*ManagerHabitacion mh1 = new ManagerHabitacion();
        ResultSet hab1;
        hab1 = mh1.getHabitaciones();
        habis.setRe(hab1);*/
        resultado = method(resultado);
        return resultado;
    }
    
    public String getRecDiaria(){
        double resultado = 0; 
        
        //fecha ayer 
        // Get today as a Calendar
        Calendar today = Calendar.getInstance();
        // Subtract 1 day
        today.add(Calendar.DATE, -1);
        // Make an SQL Date out of that
        java.sql.Date yesterday = new java.sql.Date(today.getTimeInMillis());
        
        res = mr.mostrarreservas();
        int canr = mr.getRowCount();
        RIterator resv = new RIterator(res);
        Object [] rese = (Object[]) resv.next();
        
        for(int i = 0; i < canr; i++){
            if(rese == null){
                rese = (Object []) resv.next();
                System.out.println(rese);
                
            }
            
            System.out.println(rese);
            if(rese[6] != null){
                double pre = (double) rese[6];   
                boolean reif = rese[1].equals(yesterday.toString());
                if(reif) resultado = resultado + pre;
                rese = (Object [])resv.next();
            }
            
        }
        String resu = String.valueOf(resultado);
        return resu;
    }
    
    public int cantReservas(){
        int resultado = 0;
        
        //fecha de hoy
        date = new java.sql.Date(millis);
        String date1 = date.toString();
        
        res = mr.mostrarreservas();
        int canr = mr.getRowCount();
        RIterator resv = new RIterator(res);
        Object [] rese = (Object[]) resv.next();
        
        for(int i = 0; i < canr; i++){
            boolean reif = rese[0].equals(date1);
            if(reif) resultado++;
            rese = (Object [])resv.next();
        }
        
        return resultado;
    }
    
    public String getReservasDia(){
        String resultado = "";
        
        //fecha de hoy
        date = new java.sql.Date(millis);
        String date1 = date.toString();
        
        res = mr.mostrarreservas();
        int canr = mr.getRowCount();
        RIterator resv = new RIterator(res);
        Object [] rese = (Object[]) resv.next();
        
        for(int i = 0; i < canr; i++){
            String end = rese[8].toString();
            boolean reif = rese[0].equals(date1);
            if(reif) resultado = resultado.concat(end);
            rese = (Object [])resv.next();
        }
        if(resultado == "") resultado = "No hay reservas para hoy";
        return resultado;
    }
    
    public void run(){
        //usando el iterador
        hab = mh.getHabitaciones();
        int h = mh.getRowCount();
        HIterator habis = new HIterator(hab);
        
        res = mr.mostrarreservas();
        int r = mr.getRowCount(); //cantidad de reservas
        RIterator resv = new RIterator(res);
        
        //generando fecha del dia
        date = new java.sql.Date(millis);
        String date1 = date.toString();
        
       
        try {
            String habo = getHabOcupadas(); //habitaciones ocupadas
            String recDiaria = getRecDiaria(); // recaudacion diaria
            String reservasDia = getReservasDia();
            int canReservas = cantReservas();
            
            inf.setOutputFile((this.inputFile.concat(date1)).concat(endString));
            inf.write(habo, recDiaria, habis, h, resv, r, reservasDia, canReservas);
        } catch (IOException | WriteException ex) {
            Logger.getLogger(Sistema.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(Sistema.class.getName()).log(Level.SEVERE, null, ex);
        }
       
    }
    
}
