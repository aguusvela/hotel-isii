/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotelisii;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.Iterator;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Hiplce
 */
public class RIterator implements Iterator{
private ResultSet re;
private RowProcessor convert;

    public RIterator(ResultSet rs){
        this.re = rs;
        this.convert = new BasicRowProcessor();
        
    }
    @Override
    public boolean hasNext() {
    try {
        return this.re.next();
    } catch (SQLException ex) {
        Logger.getLogger(HIterator.class.getName()).log(Level.SEVERE, null, ex);
    }   
    
        //throw new UnsupportedOperationException("Not supported yet.");
        //To change body of generated methods, choose Tools | Templates.
    return false;
    }

    @Override
    public Object next() {
    try {
        Object [] ob = null;
        if(hasNext()){            
            ob = this.convert.toArray(re);      
            java.sql.Date fii = new java.sql.Date(re.getDate("R_IN").getTime());
            java.sql.Date foo = new java.sql.Date(re.getDate("R_OUT").getTime());
            String pr = fii.toString();
            String pr2 = foo.toString();
            ob[0] = pr;
            ob[1] = pr2;
        }
        return ob;
        
    } catch (SQLException ex) {
        Logger.getLogger(HIterator.class.getName()).log(Level.SEVERE, null, ex);
    }
    return null;
    }
    public Object next(Date date){
        
    try {
        String dia = date.toString();
        if(hasNext()){
        Object [] prue;
        java.sql.Date fii = new java.sql.Date(re.getDate("R_IN").getTime());
        java.sql.Date foo = new java.sql.Date(re.getDate("R_OUT").getTime());
        String pr = fii.toString();
        String pr2 = foo.toString();
        if(pr.equals(dia)){
            prue = this.convert.toArray(re);
            prue[0] = pr;
            prue[1] = pr2;
         return prue;
        }
        }
    } catch (SQLException ex) {
        Logger.getLogger(HIterator.class.getName()).log(Level.SEVERE, null, ex);
    }
    return null;
    }
    public Object next(int num){
        
    try {
        
        if(hasNext()){
        int pr = re.getInt("R_NUMH_ID");
        if(pr == num){
         return this.convert.toArray(re);
        }
        }
    } catch (SQLException ex) {
        Logger.getLogger(HIterator.class.getName()).log(Level.SEVERE, null, ex);
    }
    return null;
    }
    public Object next(Date in, Date out){
        try {
            if(hasNext()){
            java.sql.Date fii = re.getDate("R_IN");
            java.sql.Date foo = re.getDate("R_OUT");
            java.sql.Date fi1 = new java.sql.Date(in.getTime());
            java.sql.Date fo1 = new java.sql.Date(out.getTime());
            int a,b,d,h;
            a = fi1.compareTo(fii);
            b = fi1.compareTo(foo);
            h = fo1.compareTo(fii);
            d = fo1.compareTo(foo);
            if(a==-1 && h ==1){
                return null;                 
            }
            else if(d ==1 && b==-1){
                return null;                 
            }
            else if(a==1 && d ==-1){
                return null;                 
            }            
            else if(fii.toString().equals(fi1.toString())){
                return null;
            }
            else if(foo.toString().equals(fo1.toString())){
                return null;
            }
            else{
                return this.convert.toArray(re);
            }
            }
        } catch (SQLException ex) {
            Logger.getLogger(HIterator.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
        } 

        public Object nextN(Date in, Date out){
        try {
            if(hasNext()){
            java.sql.Date fii = re.getDate("R_IN");
            java.sql.Date foo = re.getDate("R_OUT");
            java.sql.Date fi1 = new java.sql.Date(in.getTime());
            java.sql.Date fo1 = new java.sql.Date(out.getTime());
            int a,b,d,h;
            a = fi1.compareTo(fii);
            b = fi1.compareTo(foo);
            h = fo1.compareTo(fii);
            d = fo1.compareTo(foo);
            if(a==-1 && h ==1){
                return this.convert.toArray(re);                
            }
            else if(d ==1 && b==-1){
                return this.convert.toArray(re);                
            }
            else if(a==1 && d ==-1){
                return this.convert.toArray(re);               
            }            
            else if(fii.toString().equals(fi1.toString())){
                return this.convert.toArray(re);
            }
            else if(foo.toString().equals(fo1.toString())){
                return this.convert.toArray(re);
            }
            else{
                return null;
            }
            }
        } catch (SQLException ex) {
            Logger.getLogger(HIterator.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
        } 
    @Override
    public void remove() {
        //Iterator.super.remove(); //To change body of generated methods, choose Tools | Templates.
    }

    public int contador() throws SQLException{
        int i = 0;
        while(this.hasNext()){
            i++;
        }
        re.first();
        return i;
    }

    /**
     * @param re the re to set
     */
    public void setRe(ResultSet re) {
        this.re = re;
    }
}
