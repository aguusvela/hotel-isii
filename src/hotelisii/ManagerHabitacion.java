/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotelisii;

import DATABASE.ManagerBD;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

/**
 *
 * @author Mauro
 */
public class ManagerHabitacion {
    private Habitacion hb;
    private DAOH dh = new DAOH();
    public ManagerHabitacion(){
    
    }
    
   public void ModificarEstadoHabitacion(int num,int est){
       dh.UpdateEH(est, num);
   }
    public ResultSet getHabitacionesDisponibles(){
       //obtener lista de disponibles de base de datos
        ResultSet result = null;
        result = dh.ReadHD(result);
        return result;
   }
    public ResultSet getHabitaciones(){
       //obtener lista de disponibles de base de datos
        ResultSet result = null;
        result = dh.Read(result);
       return result;
   }
   public ResultSet getEstadoHabitaciones(){
       //obtener estado de habitaciones
        ResultSet result = null;
        result = dh.ReadEH(result);
        return result;
   }
   public int getRowCount(){
       ResultSet result= null;
       int i = dh.countRow(result);
       return i;
   }
   
}
