/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotelisii;

import DATABASE.ManagerBD;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Hiplce
 */
public class DAOR {
    Connection bdd = ManagerBD.connect();
    public DAOR (){}
    
    
    
    public ResultSet Read(ResultSet res){                 
        try {
            PreparedStatement st = bdd.prepareStatement("select * from RESERVA");
            res = st.executeQuery();
            //cl = new Cliente(result.getString("C_NOMBRE"),result.getString("C_APELLIDO"),result.getInt("C_DNI"),result.getString("C_DOMICILIO"),result.getString("C_PROCE"));
        }
        catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        
        return res;
        
    }
    public void Create(Reserva r){
        try {

            PreparedStatement st = bdd.prepareStatement("insert into RESERVA (R_IN, R_OUT,R_COMENTARIO,R_NUMH_ID,R_NOM_U,R_DNI_C) values (?,?,?,?,?,?)");

            java.sql.Date fii = new java.sql.Date(r.getFechaIn().getTime());
            java.sql.Date foo = new java.sql.Date(r.getFechaOut().getTime());
            

            st.setDate(1, fii);
            st.setDate(2, foo);

            st.setString(3, r.getCome());
            st.setInt(4, r.getNumh());
            st.setString(5, r.getUsu());
            st.setInt(6, r.getDni());
            st.execute();

        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }   
    }
    public void Update(int n){
        try {
            Date hoy = new Date();
            java.sql.Date date = new java.sql.Date(hoy.getTime());
            //mh.ModificarEstadoHabitacion(num, 1);
            //PreparedStatement st = bdd.prepareStatement("update HABITACION set H_ESTADO = 1 WHERE H_NROH = "+ num);
            PreparedStatement st1 = bdd.prepareStatement("update RESERVA set R_CHECKIN = "+date+" WHERE R_NUMH_ID = "+n);
            st1.executeUpdate();
            
            }
        catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }
    public void UpdateOut(int n){
        try {
            Date hoy = new Date();
            java.sql.Date date = new java.sql.Date(hoy.getTime());
            //mh.ModificarEstadoHabitacion(num, 1);
            //PreparedStatement st = bdd.prepareStatement("update HABITACION set H_ESTADO = 1 WHERE H_NROH = "+ num);
            PreparedStatement st1 = bdd.prepareStatement("update RESERVA set R_CHECKOUT = "+date+" WHERE R_NUMH_ID = "+n);
            st1.executeUpdate();
            
            }
        catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }
    public void Delete(){
        
    }
    public int countRow(ResultSet result){
    try {
        PreparedStatement st = bdd.prepareStatement("select count(*) as total from RESERVA");
        result = st.executeQuery();
        return result.getInt("total");
    } catch (SQLException ex) {
        Logger.getLogger(DAOH.class.getName()).log(Level.SEVERE, null, ex);
    }
    return 0;
    }
    public Reserva getOb(){
        return new Reserva();
    }
    
}
