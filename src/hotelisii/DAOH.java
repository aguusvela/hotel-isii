/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotelisii;

import DATABASE.ManagerBD;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Hiplce
 */
public class DAOH {
    Connection bdd = ManagerBD.connect();
    public DAOH (){}
    
    
    
    public ResultSet Read(ResultSet hab){                 
        try {
            PreparedStatement st = bdd.prepareStatement("select * from HABITACION");
            hab = st.executeQuery();
        }
        catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        
        return hab;
        
    }
    public ResultSet ReadHD(ResultSet result){
        
        try {
            PreparedStatement st = bdd.prepareStatement("select * from HABITACION where H_ESTADO = 0");
            result = st.executeQuery();
        
            
        }
        catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
       return result;
    
    }
        public ResultSet ReadEH(ResultSet result){
        
        try{
            PreparedStatement st = bdd.prepareStatement("select H_ESTADO from HABITACION");
            result = st.executeQuery();
        }catch(SQLException e){
            System.err.println(e.getMessage());
        }
       return result;
    }
    public void Create(Habitacion h){
        try {
            PreparedStatement st = bdd.prepareStatement("insert into HABITACION values (?,?,?,?,?)");
            st.setInt(1, h.getEstado());
            Tipo t = h.getTipo();
            
            if(t.equals(Tipo.SIMPLE)){
                st.setInt(2, 1);
               }
            if(t.equals(Tipo.DOBLE)){
                st.setInt(2, 2);
               }
            if(t.equals(Tipo.TRIPLE)){
                st.setInt(2, 3);
               }
            if(t.equals(Tipo.CUADRUPLE)){
                st.setInt(2, 4);
               }
            if(t.equals(Tipo.QUINTUPLE)){
                st.setInt(2, 5);
               }
            st.setBoolean(3, h.isMatrimonial());
            st.setFloat(4, h.getPrecio());
            st.setInt(5, h.getNumHabitacion());
            st.execute();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }   
    }
    public void Update(int n){
        try {
            PreparedStatement st = bdd.prepareStatement("update HABITACION set H_ESTADO = 1 WHERE H_NROH = "+ n);
            
            st.executeUpdate();
            
            }
        catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }
    public void UpdateEH(int e,int n){
        try {
            PreparedStatement st = bdd.prepareStatement("update HABITACION set H_ESTADO = "+e+" WHERE H_NROH = "+ n);
            st.executeUpdate();
            }
        catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }
    public void Delete(){
        
    }
    public int countRow(ResultSet result){
        try {
            PreparedStatement st = bdd.prepareStatement("select count(*) as total from HABITACION");
            result = st.executeQuery();
            return result.getInt("total");
        } catch (SQLException ex) {
            Logger.getLogger(DAOH.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }
    
}
