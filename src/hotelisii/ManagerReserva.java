/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotelisii;

import DATABASE.ManagerBD;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Hiplce
 */
public class ManagerReserva {
    
    private Reserva re;
    private ManagerHabitacion mh = new ManagerHabitacion();
    private DAOR dao = new DAOR();
    
    /*public String fechaActual(){
        Date fecha = new Date();
        SimpleDateFormat formato = new SimpleDateFormat("MM/dd/YYYY");
        return formato.format(fecha);
    }*/
    public ManagerReserva(){
    }
    public void GenerarReserva(Date fi,Date fo,int num,String come, String usu, int dni){

         //re = new Reserva(fi,fo,num,come,usu,dni);
         re = dao.getOb();
         re.setFechaIn(fi);
         re.setFechaOut(fo);
         re.setNumh(num);
         re.setCome(come);
         re.setUsu(usu);
         re.setDni(dni);
         dao.Create(re);
    }
       public ResultSet mostrarreservas(){
        ResultSet result = null;
        result = dao.Read(result);
        return result;
      }
       public void updateCheckIn(int num){
            dao.Update(num);   
      }
         public void updateCheckOut(int num){
            dao.UpdateOut(num);   
      }
      public int getRowCount(){
       ResultSet result= null;
       int i = dao.countRow(result);
       return i;
   }
      public int [] Estadistica() throws SQLException{
          int [] meses = new int [12];
          ResultSet result = null;
          Calendar cal = Calendar.getInstance();
          
          for(int i = 0;i<12;i++){
              meses[i] = 0;
          }
          result = dao.Read(result);
          while(result.next())
          {
           Date dia = result.getDate("R_IN");
           cal.setTime(dia);
           int mes = cal.get(Calendar.MONTH);
           meses[mes]++;
           
          }
          
          return meses;
      }
      

}
