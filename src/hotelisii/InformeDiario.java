/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotelisii;

import java.lang.*;


import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.Locale;

import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Colour;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

/**
 *
 * @author Mauro
 */
public class InformeDiario {

    private WritableCellFormat timesBoldUnderline;
    private WritableCellFormat times;
    private WritableCellFormat times1;
    private WritableCellFormat times2;
    private WritableCellFormat times3;
    private String inputFile;
    private final long millis=System.currentTimeMillis();  
    private final java.sql.Date date = new java.sql.Date(millis);  
    
    public void setOutputFile(String inputFile) {
        this.inputFile = inputFile;
    }

    public void write(String habOcupadas, String recDiaria, HIterator hab, int cHab, RIterator resv, int cRes, String reservasDia, int canReservas) throws IOException, WriteException, SQLException {
        File file = new File(inputFile);
        WorkbookSettings wbSettings = new WorkbookSettings();

        wbSettings.setLocale(new Locale("en", "EN"));

        WritableWorkbook workbook = Workbook.createWorkbook(file, wbSettings);
        workbook.createSheet("Report", 0);
        WritableSheet excelSheet = workbook.getSheet(0);
        createLabel(excelSheet, hab, cHab, resv, cRes);
        createContent(excelSheet, habOcupadas, recDiaria, canReservas, reservasDia);
        workbook.write();
        workbook.close();
    }

    private void createLabel(WritableSheet sheet, HIterator hab, int cHab, RIterator resv, int cRes) throws WriteException, SQLException {
        // Creo el formato de la fuente
        WritableFont times20pt = new WritableFont(WritableFont.TIMES, 20);
        times20pt.setColour(Colour.BLUE);
        times = new WritableCellFormat(times20pt);
        times.setWrap(true);
        
        //Creo el formato times1
        WritableFont times20pt1 = new WritableFont(WritableFont.TIMES, 20);
        times20pt1.setColour(Colour.RED);
        times1 = new WritableCellFormat(times20pt1);
        times1.setWrap(true);
        
        //create a bold font with underlines
        WritableFont times10ptBoldUnderline = new WritableFont(
                WritableFont.TIMES, 10, WritableFont.BOLD);
        timesBoldUnderline = new WritableCellFormat(times10ptBoldUnderline);
        // Lets automatically wrap the cells
        timesBoldUnderline.setWrap(true);
        
        //Fuente azul oscura 
        WritableFont font18 = new WritableFont(WritableFont.TIMES, 18);
        font18.setColour(Colour.DARK_BLUE2);
        times2 = new WritableCellFormat(font18);
        times2.setWrap(true);
        
        //Fuente verde oscura
        WritableFont font18v = new WritableFont(WritableFont.TIMES, 18);
        font18v.setColour(Colour.DARK_GREEN);
        times3 = new WritableCellFormat(font18v);
        times3.setWrap(true);

        CellView cv = new CellView();
        
        //agrego un tipo de formato para usar mas tarde
        cv.setFormat(times); 
        cv.setFormat(times1);
        cv.setFormat(times2);
        cv.setFormat(times3);
        cv.setFormat(timesBoldUnderline);
        
        
        cv.setAutosize(true);
        sheet.setColumnView(0, cv); //seteo las dimenciones de la celda
        sheet.setColumnView(1, cv);
        sheet.setColumnView(2, cv);
        sheet.setColumnView(3, cv);
        sheet.setColumnView(4, cv);
        

        // Escribo los headers
        addCaption(sheet, 0, 0, "Informe Diario del dia\n".concat(date.toString()), times1);
        addCaption(sheet, 1, 0, "Recaudacion del dia anterior", times);
        addCaption(sheet, 2, 0, "Habitaciones reservadas para el dia", times);
        addCaption(sheet, 3, 0, "Cantidad de reservas para el dia", times);
        addCaption(sheet, 4, 0, "Habitaciones ocupadas", times);
        addCaption(sheet, 0, 8, "Estado Habitaciones", times2);
        addCaption(sheet, 1, 8, "Repaso", times3);
        addCaption(sheet, 2, 8, "Rearmar", times3);
        addCaption(sheet, 3, 8, "Preparar", times3);
        
        //Estado de limpieza de las habitaciones 
        int i = 9;
        boolean sale; // variable de ayuda para determinar si el cliente entra hoy a la habitacion
        boolean result;
        Object [] habs = (Object[]) hab.next();
        Object [] res = (Object[]) resv.next();
        
        for(int b = 0; b < cHab; b++){
            sale = false;
            for(int iR = 0; iR < cRes; iR++){
                String st1 = (String) res[0];
                String st2 = date.toString();
                result = st1.equals(st2);
                if(((int) res[8] == (int) habs[4]) && (result)) sale = true;
                res = (Object[]) resv.next();
            }
            
            //reinicio el mostrar reservas
            ManagerReserva mr = new ManagerReserva();
            ResultSet res1;
            res1 = mr.mostrarreservas();
            resv.setRe(res1);
            res = (Object[]) resv.next();
            
            //agrego la habitacion a la lista en excell
            String num =(habs[4]).toString();
            addCaption(sheet, 0, i, "Habitacion numero ".concat(num), times3);
            //Marco según el estado de limpieza
            if(sale){ // habitacion a preparar (dia check In)
                addCaption(sheet, 3, i, "x", times1);
            }else{
                if((int) habs[0] == 0 || (int) habs[0] == 1){ //habitacion para repaso (Ocupada, desocupada)
                    addCaption(sheet, 1, i, "x", times1);   
                }
                else if((int) habs[0] == 2){ //habitacion para rearmar (Check out)
                    addCaption(sheet, 2, i, "x", times1);
                }
            }
            i++;
            habs = (Object[]) hab.next();
        }
        
    }

    private void createContent(WritableSheet sheet, String habOcupadas, String recDiaria, int canRes, String reservasDia) throws WriteException,
            RowsExceededException {
        
        addLabel(sheet, 1, 1, recDiaria);
        addLabel(sheet, 4, 1, habOcupadas);
        addNumber(sheet, 3, 1, canRes);
        addLabel(sheet, 2, 1, reservasDia);

    }

    private void addCaption(WritableSheet sheet, int column, int row, String s, WritableCellFormat format)
            throws RowsExceededException, WriteException {
        Label label;
        label = new Label(column, row, s, format);
        sheet.addCell(label);
    }

    private void addNumber(WritableSheet sheet, int column, int row,
            Integer integer) throws WriteException, RowsExceededException {
        Number number;
        number = new Number(column, row, integer, timesBoldUnderline);
        sheet.addCell(number);
    }

    private void addLabel(WritableSheet sheet, int column, int row, String s)
            throws WriteException, RowsExceededException {
        Label label;
        label = new Label(column, row, s, timesBoldUnderline);
        sheet.addCell(label);
    }
   /*public static void main(String[] args) throws WriteException, IOException {
        String inputFile = "C:\\Users\\Itachi\\Desktop\\hotelsito\\HotelISII1\\newFile.xls";
        
        
        InformeDiario inf = new InformeDiario();

        inf.setOutputFile(inputFile);
        inf.write();

    }*/
}