/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotelisii;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Iterator;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Hiplce
 */
public class HIterator implements Iterator{
private ResultSet re;
private RowProcessor convert;

    public HIterator(ResultSet rs){
        this.re = rs;
        this.convert = new BasicRowProcessor();
        
    }
    @Override
    public boolean hasNext() {
    try {
        return this.re.next();
    } catch (SQLException ex) {
        Logger.getLogger(HIterator.class.getName()).log(Level.SEVERE, null, ex);
    }   
    
        //throw new UnsupportedOperationException("Not supported yet.");
        //To change body of generated methods, choose Tools | Templates.
    return false;
    }
    

    @Override
    public Object next() {
    try {
        Object [] ob = null;
        if(hasNext()){            
            ob = this.convert.toArray(re);                 
        }
        return ob;
        
    } catch (SQLException ex) {
        Logger.getLogger(HIterator.class.getName()).log(Level.SEVERE, null, ex);
    }
    return null;
    }
    public Object next(int tipo){
        
    try {
        
        if(hasNext()){
        int pr = re.getInt("H_TIPO");
        if(pr == tipo){
         return this.convert.toArray(re);
        }
        }
    } catch (SQLException ex) {
        Logger.getLogger(HIterator.class.getName()).log(Level.SEVERE, null, ex);
    }
    return null;
    }

    @Override
    public void remove() {
        //Iterator.super.remove(); //To change body of generated methods, choose Tools | Templates.
    }

    public int contador() throws SQLException{
        int i = 0;
        while(this.hasNext()){
            i++;
        }
        re.first();
        return i;
    }

    /**
     * @param re the re to set
     */
    public void setRe(ResultSet re) {
        this.re = re;
    }
}
