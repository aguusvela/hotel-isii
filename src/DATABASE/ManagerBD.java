package DATABASE;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author Hiplce
 */
public class ManagerBD {
    Statement consulta;
    private static String ruta;
    private static Connection connect = null;
    
    private static void crearInstancia(){
    if(connect == null){
        try {
            ruta = "C:\\Users\\Hiplce\\Desktop\\hotelsito\\HotelISII1\\datos\\hotel.bd";//cambiar ruta
            connect = DriverManager.getConnection("jdbc:sqlite:"+ruta);
            if (connect!=null) {
                System.out.println("Conectado");
            }
        }catch (SQLException ex) {
            System.err.println("No se ha podido conectar a la base de datos\n"+ex.getMessage());
        }
       }
    }

    public static Connection connect(){
        if(connect == null){
            crearInstancia();
       }
        return connect;
    }
        public void close(){
               try {
                   connect.close();
               } catch (SQLException ex) {
                   Logger.getLogger(ManagerBD.class.getName()).log(Level.SEVERE, null, ex);
               }
        }
}
