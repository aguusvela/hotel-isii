/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import GUI.Main;
import hotelisii.Cliente;
import hotelisii.HIterator;
import hotelisii.ManagerCliente;
import hotelisii.ManagerHabitacion;
import hotelisii.ManagerReserva;
import hotelisii.RIterator;
import hotelisii.Usuario;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import static java.lang.Integer.parseInt;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import static javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Hiplce
 */
public class ReservarHabitacion extends javax.swing.JFrame {

    /**
     * Creates new form ReservarHabitacion
     */
    DefaultTableModel dtm0 = new DefaultTableModel(){
            @Override
    public boolean isCellEditable(int row, int column)
    {
      return false;//This causes all cells to be not editable
    }
  
            @Override
            public Class getColumnClass(int columna) {
		if (columna == 2)
                    return Long.class; //Le dice al modelo que la primera columna es de tipo integer
		return String.class; //Si no, es String
            }
    };
        private void CerrarVentana(){
          addWindowListener(new WindowAdapter() {
             @Override
             public void windowClosing(WindowEvent e) {
                int confirm = JOptionPane.showOptionDialog(
                null, "¿Está seguro que desea salir? ", 
                 "Salir", JOptionPane.YES_NO_OPTION,  
                JOptionPane.QUESTION_MESSAGE, null, null, null);
                if (confirm == 0) {
                System.exit(0);
                }
                else{
                    setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
                }
             }
          });
        
    }

        
        Usuario u = new Usuario("agu","agu",1);
        ManagerReserva mr = new ManagerReserva();
        ManagerCliente mc = new ManagerCliente();
        ManagerHabitacion mh = new ManagerHabitacion();
        HIterator hi;
    public ReservarHabitacion() throws SQLException {

        initComponents();
        int canthabs = mh.getRowCount();
        jLabelCantHabs.setText("Cantidad de Habitaciones : "+canthabs);
        CerrarVentana();
        this.setLocationRelativeTo(null);
        Date hoy = new Date();
        jDateChooserin.setMinSelectableDate(hoy);
        dtm0.addColumn("Habitacion");
        dtm0.addColumn("Tipo");
        newClient.add(Sicn);
        newClient.add(nocn);
        matrimonial.add(siM);
        matrimonial.add(noM);
        this.jTable1.setModel(dtm0);
        jComboBoxcliente.setEnabled(true);
        jTextFieldape.setEnabled(false);
        jTextFieldnom.setEnabled(false);
        jTextFielddom.setEnabled(false);
        jTextFielddoc.setEnabled(false);
        jComboBoxproce.setEnabled(false);
        
        nocn.setSelected(true);
        noM.setSelected(true);
        ResultSet clie;
        clie = mc.ObtenerClientes();
        while(clie.next()){
            jComboBoxcliente.addItem(clie.getString("C_NOMBRE")+ " " + clie.getString("C_APELLIDO"));
        }
        
        
        //CargarTabla();
        
    }
    public void Reservar() throws SQLException, Exception{
        String nomi = "",ape = "",dom = "",proce;
            int doc = 0;
        Cliente c = null;
        Exception p = new Exception();
        int noc = 0;
        if (Sicn.isSelected()){
            
            try{
            if (!"".equals(jTextFieldnom.getText())){
                nomi = jTextFieldnom.getText();
               
            }
            else {                
                throw p;
            }
            }
            catch(Exception e){
                JOptionPane.showMessageDialog(null,"Ingrese nombre");
                noc = 1;
            }
            try{
            if (!"".equals(jTextFieldape.getText())){
                ape = jTextFieldape.getText();

            }
            else {
                
                throw p;
            }
            }
            catch(Exception e){
                JOptionPane.showMessageDialog(null,"Ingrese apellido");
                noc = 1;
            }try{
            if (!"".equals(jTextFielddoc.getText())){
                doc = parseInt(jTextFielddoc.getText());
               
            }
            else {
                throw p;
            }
            }
            catch(Exception e){
                JOptionPane.showMessageDialog(null,"Ingrese documento");
                noc = 1;
            }try{
            if (!"".equals(jTextFielddom.getText())){
                dom = jTextFielddom.getText();
        
            } else {
                
                throw p;
            }
            }
            catch(Exception e){
                JOptionPane.showMessageDialog(null,"Ingrese domicilio");
                noc = 1;
            }
            proce = (String) jComboBoxproce.getSelectedItem();
            
            if(!"".equals(nomi)&&!"".equals(ape)&&!"".equals(dom)&&doc != 0){
                mc.CrearCiente(nomi, ape, doc, dom, proce);
                //c = new Cliente(nomi, ape, doc, dom, proce);
                
                //meterlo en la base de datos
            }
                    
        }

        if(nocn.isSelected()){
            try{
            if (!"Seleccione Cliente".equals((String)jComboBoxcliente.getSelectedItem())){
                String clie = (String) jComboBoxcliente.getSelectedItem();
                String part [] = clie.split(" ");
                c = mc.ObtenerCliente(part[0], part[1]);
            }
            else{
                throw p;
            }
            }catch(Exception e){ 
                JOptionPane.showMessageDialog(null, "seleccione un cliente");
                noc = 1;
            }
        }
        Date in = null;
        Date out = null;
        try{
            in = jDateChooserin.getDate();
            if (in == null){
                Exception e = new Exception();
                throw e;
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Seleccione la fecha de in");
            noc = 1;
        }
        try{
            out = jDateChooserout.getDate();
            if (in == null){
                Exception e = new Exception();
                throw e;
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Seleccione la fecha de out");
            noc = 1;
        }
            
        
        // sacar numero de habitacion
        //int col = jTable1.getSelectedColumn();
        int fil = jTable1.getSelectedRow();
       
        int numh = -1;
        try{
            if (fil == -1){
                Exception e = new Exception();
                throw e;
            }
            else{
                numh = (int) jTable1.getValueAt(fil, 0);
            }
        } catch (Exception e) {
            noc = 1;
            JOptionPane.showMessageDialog(null, "Seleccione la habitacion");
        }
        
        String comen = jTextArea1.getText();
        
        
        /*while(asd.next()){

            if(asd.getInt("R_NUMH_ID") == numh){ 
                java.sql.Date fii = asd.getDate("R_IN");
                java.sql.Date foo = asd.getDate("R_OUT");
                a = fi1.compareTo(fii);
                b = fi1.compareTo(foo);
                try{
                    if(a==1 && b ==-1){
                        
                        throw p;                    
                    }
                    h = fo1.compareTo(fii);
                    d = fo1.compareTo(foo);
                    if(h==1 && d ==-1){
                        
                        throw p;                    
                    }
                    if (a == 0){
                        
                        throw p;
                    }
                }
                catch(Exception e){
                    JOptionPane.showMessageDialog(null, "hay una reserva en esta fecha para esta habitacion");
                    noc = 1;
                    break;
                }
            }
        }*/
        
        try{
        if(noc ==0){
            mr.GenerarReserva(in, out, numh, comen, u.getNombre(), c.getDoc());
            JOptionPane.showMessageDialog(null, "Reserva Exitosa");
        }
        else
            throw p;
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "No se pudo cargar la reserva");
        }

        
        //va a la base de datos.
    }
    public void limpiarTabla(){
    try {

    while (dtm0.getRowCount() > 0) 
    {
    dtm0.removeRow(0);
    }
    } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al limpiar la tabla.");
        }
    }
 /*   public void MostrarSimplesnM() throws SQLException{
        ResultSet hab;
        hab = mh.getHabitaciones();
        limpiarTabla();
        while(hab.next()){
            
               Object [] fila = new Object[2]; // Hay tres columnas en la tabla
               fila[0] = hab.getObject("H_NROH");
               if(hab.getInt("H_TIPO") == 1){
                fila[1] = "SIMPLE";
                dtm0.addRow(fila);
               }
        }
    }
        public void MostrarDoblesnM() throws SQLException{
        ResultSet hab;
        hab = mh.getHabitaciones();
        limpiarTabla();
        while(hab.next()){
            
               Object [] fila = new Object[2]; // Hay tres columnas en la tabla
               fila[0] = hab.getObject("H_NROH");
               if(hab.getInt("H_TIPO") == 2 && hab.getBoolean("H_MATRIMONIAL") == false){
                fila[1] = "DOBLE";
                dtm0.addRow(fila);
               }
        }
    }
        public void MostrarTriplesnM() throws SQLException{
        ResultSet hab;
        hab = mh.getHabitaciones();
        limpiarTabla();
        while(hab.next()){
            
               Object [] fila = new Object[2]; // Hay tres columnas en la tabla
               fila[0] = hab.getObject("H_NROH");
               if(hab.getInt("H_TIPO") == 3 && hab.getBoolean("H_MATRIMONIAL") == false){
                fila[1] = "TRIPLE";
                dtm0.addRow(fila);
               }
        }
    }
        public void MostrarCuadruplesnM() throws SQLException{
        ResultSet hab;
        hab = mh.getHabitaciones();
        limpiarTabla();
        while(hab.next()){
            
               Object [] fila = new Object[2]; // Hay tres columnas en la tabla
               fila[0] = hab.getObject("H_NROH");
               if(hab.getInt("H_TIPO") == 4 && hab.getBoolean("H_MATRIMONIAL") == false){
                fila[1] = "CUADRUPLE";
                dtm0.addRow(fila);
               }
        }
    }
        public void MostrarQuintuplesnM() throws SQLException{
        ResultSet hab;
        hab = mh.getHabitaciones();
        limpiarTabla();
        while(hab.next()){
            
               Object [] fila = new Object[2]; // Hay tres columnas en la tabla
               fila[0] = hab.getObject("H_NROH");
               if(hab.getInt("H_TIPO") == 5 && hab.getBoolean("H_MATRIMONIAL") == false){
                fila[1] = "QUINTUPLE";
                dtm0.addRow(fila);
               }
        }
    }
         public void MostrarDoblesM() throws SQLException{
        ResultSet hab;
        hab = mh.getHabitaciones();
        limpiarTabla();
        while(hab.next()){
            
               Object [] fila = new Object[2]; // Hay tres columnas en la tabla
               fila[0] = hab.getObject("H_NROH");
               if(hab.getInt("H_TIPO") == 2 && hab.getBoolean("H_MATRIMONIAL") == true){
                fila[1] = "DOBLE";
                dtm0.addRow(fila);
               }
        }
    }
        public void MostrarTriplesM() throws SQLException{
        ResultSet hab;
        hab = mh.getHabitaciones();
        limpiarTabla();
        while(hab.next()){
            
               Object [] fila = new Object[2]; // Hay tres columnas en la tabla
               fila[0] = hab.getObject("H_NROH");
               if(hab.getInt("H_TIPO") == 3 && hab.getBoolean("H_MATRIMONIAL") == true){
                fila[1] = "TRIPLE";
                dtm0.addRow(fila);
               }
        }
    }
        public void MostrarCuadruplesM() throws SQLException{
        ResultSet hab;
        hab = mh.getHabitaciones();
        limpiarTabla();
        while(hab.next()){
            
               Object [] fila = new Object[2]; // Hay tres columnas en la tabla
               fila[0] = hab.getObject("H_NROH");
               if(hab.getInt("H_TIPO") == 4&& hab.getBoolean("H_MATRIMONIAL") == true){
                fila[1] = "CUADRUPLE";
                dtm0.addRow(fila);
               }
        }
    }
        public void MostrarQuintuplesM() throws SQLException{
        ResultSet hab;
        hab = mh.getHabitaciones();
        limpiarTabla();
        while(hab.next()){
            
               Object [] fila = new Object[2]; // Hay tres columnas en la tabla
               fila[0] = hab.getObject("H_NROH");
               if(hab.getInt("H_TIPO") == 5&& hab.getBoolean("H_MATRIMONIAL") == true){
                fila[1] = "QUINTUPLE";
                dtm0.addRow(fila);
               }
        }
    }*/
    
    public void CargarTabla() throws SQLException{
        ResultSet habs;
        habs = mh.getHabitaciones();
        limpiarTabla();
        while (habs.next())
            {
               Object [] fila = new Object[2]; // Hay tres columnas en la tabla
               fila[0] = habs.getObject("H_NROH");
               if(habs.getInt("H_TIPO") == 1){
                fila[1] = "SIMPLE";
               }
               if(habs.getInt("H_TIPO") == 2){
                fila[1] = "DOBLE";
               }
               if(habs.getInt("H_TIPO") == 3){
                fila[1] = "TRIPLE";
               }
               if(habs.getInt("H_TIPO") == 4){
                fila[1] = "CUADRUPLE";
               }
               if(habs.getInt("H_TIPO") == 5){
                fila[1] = "QUINTUPLE";
               }
               dtm0.addRow(fila); 
            }
    
    }
    
    public void filtrarFecha2(Date i,Date o){
        ResultSet rs = mr.mostrarreservas();
        RIterator ri = new RIterator(rs);
        ResultSet rh = mh.getHabitaciones();
        HIterator habis = new HIterator(rh);
        Object [] habs = (Object[]) habis.next();
        //Object [] reser = (Object[]) ri.next();
        int canth = mh.getRowCount();
        int cantr = mr.getRowCount();
        int hab,tipo = 0;
        limpiarTabla();
        
        int [] habsnres = new int [100];
        for(int p = 0;p<100;p++){
            habsnres[p] = 0;
        }
        for (int j = 0;j<canth;j++){
            habsnres[j] = (int) habs[4];
            habs = (Object[]) habis.next();
        }
        rh = mh.getHabitaciones();
        habis.setRe(rh);
        if (i != null && o != null){
            Object [] res = (Object[]) ri.nextN(i, o);
            Object [] fila = new Object [2];
            
            for(int j = 0 ;j<cantr;j++){
                if (res == null){
                    res = (Object[]) ri.nextN(i, o);
                }
                else {
                    hab = (int) res [8];
                    //habsres[j] = hab;
                    
                    for(int p = 0;p<cantr;p++){
                        for(int h = 0;h<canth;h++){
                            if(hab == habsnres[h]){
                                habsnres[h] = 0;
                            }
                        }
                    }
                    res = (Object[]) ri.nextN(i, o);
                }
            }
            int exito=1;
            habs = (Object[]) habis.next();
            for(int p = 0;p<canth;p++){
                exito=0;
                hab = (int) habs[4];
                for(int q = 0;q<canth;q++){
                    if(hab == habsnres[q]){
                        tipo = (int) habs[1];
                        exito=1;
                    }                   
                }if(exito == 1){
                fila[0]= hab;
                if(tipo == 1){
                 fila[1] = "SIMPLE";
                }
                if(tipo == 2){
                 fila[1] = "DOBLE";
                }
                if(tipo == 3){
                 fila[1] = "TRIPLE";
                }
                if(tipo == 4){
                 fila[1] = "CUADRUPLE";
                }
                if(tipo == 5){
                 fila[1] = "QUINTUPLE";
                }
                String tipoC;
                tipoC =(String) jComboBox3.getSelectedItem();
                System.out.println("Tipo Fila: "+fila[1]);
                System.out.println("Tipo combo: " +tipoC);
                
                
                if(siM.isSelected()){
                    if((int)habs[2] == 1){
                        if(tipoC.equals(fila[1])){
                            dtm0.addRow(fila);
                        }
                    }
                }
                if(noM.isSelected()){
                    if((int)habs[2] == 0)
                        if(tipoC.equals(fila[1])){
                            dtm0.addRow(fila);
                        }
                    }
                }
                
                habs = (Object[]) habis.next();
            }
        
        
        
    }
    }
    /*public void filtrarFecha(Date i, Date o){
        int hab;
        ResultSet rs = mr.mostrarreservas();
        RIterator ri = new RIterator(rs);
        ResultSet rh = mh.getHabitaciones();
        HIterator habis = new HIterator(rh);
        Object [] habs = (Object[]) habis.next();
        Object [] reser = (Object[]) ri.next();
        int  [] habsres = new int [100];
        int [] habsnres = new int [100];
        for(int p = 0;p<100;p++){
            habsres[p] = 0;
        }
        int existe=0;
        int tipo = 0;
        int h = mr.getRowCount();
        int f = mh.getRowCount();
        for(int b = 0;b<h;b++){
            existe = 0;
            for(int q = 0;q<h;q++){
                if((int)reser[8] == habsres[q]){
                    existe = 1;
                    break;
                }
            }
            if(existe == 0){
            habsres[b] = (int) reser[8];
            System.out.println("Habs con reserva"+ reser[8]);
            reser = (Object[]) ri.next();
            }
        }
        for(int y = 0;y<f;y++){
            for(int m = 0;m<100;m++){
                existe = 0;
                if((int)habs [4] == habsres[m]){
                    existe = 1;
                    break;
                }                
            }
            if(existe == 0){
                habsnres[y] = (int) habs[4];
                System.out.println("Habs sin reserva"+ habs[4]);
                habs = (Object[]) habis.next();
            }
        }
        int j;
        rh = mh.getHabitaciones();
        habis.setRe(rh);
        rs = mr.mostrarreservas();
        ri.setRe(rs);
        limpiarTabla();
        if (i != null && o != null){
            Object [] res = (Object[]) ri.next(i, o);
           /* if(res !=null){
                hab = (int) res [8];
            }
            Object [] fila = new Object [2];
            
            for(j = 0 ;j<h;j++){
                if (res == null){
                    res = (Object[]) ri.next(i, o);
                }
                else {
                    hab = (int) res [8];
                    //habsres[j] = hab;
                    
                    for(int p = 0;p<f;p++){
                        int num = (int) habs[4];                        
                        if(num == hab){
                            tipo = (int) habs[1];
                        }
                        habs = (Object []) habis.next();
                    }
                    rh = mh.getHabitaciones();

                    habis.setRe(rh);
                    habs = (Object[]) habis.next();
                    fila[0]= hab;
                    if(tipo == 1){
                    fila[1] = "SIMPLE";
                   }
                   if(tipo == 2){
                    fila[1] = "DOBLE";
                   }
                   if(tipo == 3){
                    fila[1] = "TRIPLE";
                   }
                   if(tipo == 4){
                    fila[1] = "CUADRUPLE";
                   }
                   if(tipo == 5){
                    fila[1] = "QUINTUPLE";
                   }
                   for(int w = 0;w<dtm0.getRowCount();w++){
                        existe=0;
                        if(jTable1.getValueAt(w, 0)==fila[0]){
                            existe = 1;
                            break;
                        }                        
                   }
                   if(existe==0){
                       dtm0.addRow(fila);
                   }
                   //dtm0.addRow(res);
                    res = (Object[]) ri.next(i, o);
                }
            }
            rh = mh.getHabitaciones();
            habis.setRe(rh);
            habs = (Object []) habis.next();
            f = mh.getRowCount();
            int nor = 1;
            for(int t = 0;t<f;t++){
                nor =1;
                int com = (int) habs[4];
                for(int a = 0;a<habsnres.length;a++){
                    if(habsnres[a]== 0){
                        continue;
                    }
                    if(com == habsnres[a])
                        nor=0;
                }
                if (nor == 1){
                    fila[0] = com;
                    tipo = (int) habs[1];
                    if(tipo == 1){
                    fila[1] = "SIMPLE";
                   }
                   if(tipo == 2){
                    fila[1] = "DOBLE";
                   }
                   if(tipo == 3){
                    fila[1] = "TRIPLE";
                   }
                   if(tipo == 4){
                    fila[1] = "CUADRUPLE";
                   }
                   if(tipo == 5){
                    fila[1] = "QUINTUPLE";
                   }
                   dtm0.addRow(fila);
                 }
                habs = (Object[]) habis.next();
            }
            
        }
    }*/
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        newClient = new javax.swing.ButtonGroup();
        matrimonial = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        Sicn = new javax.swing.JRadioButton();
        nocn = new javax.swing.JRadioButton();
        jComboBoxcliente = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jComboBoxproce = new javax.swing.JComboBox<>();
        jTextFieldnom = new javax.swing.JTextField();
        jTextFieldape = new javax.swing.JTextField();
        jTextFielddoc = new javax.swing.JTextField();
        jTextFielddom = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jDateChooserin = new com.toedter.calendar.JDateChooser();
        jLabel10 = new javax.swing.JLabel();
        jDateChooserout = new com.toedter.calendar.JDateChooser();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jComboBox3 = new javax.swing.JComboBox<>();
        siM = new javax.swing.JRadioButton();
        noM = new javax.swing.JRadioButton();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jLabel15 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabelCantHabs = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);

        jPanel1.setBackground(new java.awt.Color(32, 56, 86));
        jPanel1.setPreferredSize(new java.awt.Dimension(800, 600));

        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Cliente nuevo?");

        Sicn.setBackground(new java.awt.Color(32, 56, 86));
        Sicn.setForeground(new java.awt.Color(255, 255, 255));
        Sicn.setText("Si");
        Sicn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SicnActionPerformed(evt);
            }
        });

        nocn.setBackground(new java.awt.Color(32, 56, 86));
        nocn.setForeground(new java.awt.Color(255, 255, 255));
        nocn.setText("No");
        nocn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nocnActionPerformed(evt);
            }
        });

        jComboBoxcliente.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccione Cliente" }));
        jComboBoxcliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxclienteActionPerformed(evt);
            }
        });

        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Seleccione cliente existente:");

        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Cliente:");

        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Nombre");

        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Apellido");

        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Documento");

        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("Domicilio");

        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("Procedencia");

        jComboBoxproce.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Buenos Aires", "Catamarca", "Chaco", "Chubut", "Cordoba", "Corrientes", "Entre Rios", "Formosa", "Jujuy", "La Pampa", "La Rioja", "Mendoza", "Misiones", "Neuquen", "Rio Negro", "Salta", "San Juan", "San Luis", "Santa Cruz", "Santa Fe", "Santiago del Estero", "Tierra del Fuego", "Tucuman", "Otro" }));

        jTextFieldnom.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldnomActionPerformed(evt);
            }
        });
        jTextFieldnom.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextFieldnomKeyTyped(evt);
            }
        });

        jTextFieldape.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextFieldapeKeyTyped(evt);
            }
        });

        jTextFielddoc.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextFielddocKeyTyped(evt);
            }
        });

        jTextFielddom.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextFielddomKeyTyped(evt);
            }
        });

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setText("Datos Reserva");

        jDateChooserin.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jDateChooserinPropertyChange(evt);
            }
        });

        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setText("Fecha In");

        jDateChooserout.setMinSelectableDate(jDateChooserin.getDate());
        jDateChooserout.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jDateChooseroutPropertyChange(evt);
            }
        });

        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("Fecha Out");

        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        jLabel12.setText("Tipo de Habitacion");

        jComboBox3.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "SIMPLE", "DOBLE", "TRIPLE", "CUADRUPLE", "QUINTUPLE" }));
        jComboBox3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox3ActionPerformed(evt);
            }
        });
        jComboBox3.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jComboBox3PropertyChange(evt);
            }
        });

        siM.setBackground(new java.awt.Color(32, 56, 86));
        siM.setForeground(new java.awt.Color(255, 255, 255));
        siM.setText("Si");
        siM.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                siMActionPerformed(evt);
            }
        });

        noM.setBackground(new java.awt.Color(32, 56, 86));
        noM.setForeground(new java.awt.Color(255, 255, 255));
        noM.setText("No");
        noM.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                noMActionPerformed(evt);
            }
        });

        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setText("Matrimonial?");

        jLabel14.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(255, 255, 255));
        jLabel14.setText("Habitaciones");

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jTable1.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_ALL_COLUMNS);
        jTable1.setCellSelectionEnabled(true);
        jScrollPane1.setViewportView(jTable1);
        jTable1.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_INTERVAL_SELECTION);

        jButton1.setText("Borrar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Cargar");
        jButton2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton2MouseClicked(evt);
            }
        });
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane2.setViewportView(jTextArea1);

        jLabel15.setForeground(new java.awt.Color(255, 255, 255));
        jLabel15.setText("Comentarios");

        jLabel17.setFont(new java.awt.Font("sansserif", 0, 30)); // NOI18N
        jLabel17.setForeground(new java.awt.Color(204, 0, 0));
        jLabel17.setText("X");
        jLabel17.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel17MouseClicked(evt);
            }
        });

        jLabel18.setIcon(new javax.swing.ImageIcon(getClass().getResource("/GUI/icons8-volver-30.png"))); // NOI18N
        jLabel18.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel18MouseClicked(evt);
            }
        });

        jLabelCantHabs.setForeground(new java.awt.Color(255, 255, 255));
        jLabelCantHabs.setText("Cantidad de Habitaciones : ");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(44, 640, Short.MAX_VALUE)
                .addComponent(jButton1)
                .addGap(35, 35, 35)
                .addComponent(jButton2)
                .addGap(100, 100, 100))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(52, 52, 52)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jComboBoxproce, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel8)
                            .addComponent(jLabel7)
                            .addComponent(jLabel6)
                            .addComponent(jLabel5)
                            .addComponent(jLabel1)
                            .addComponent(nocn)
                            .addComponent(Sicn)
                            .addComponent(jLabel2)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(jLabel4)
                                .addComponent(jLabel3))
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(jTextFielddoc, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jTextFieldape, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jTextFieldnom, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jComboBoxcliente, javax.swing.GroupLayout.Alignment.LEADING, 0, 126, Short.MAX_VALUE))
                            .addComponent(jTextFielddom, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(137, 137, 137)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(5, 5, 5)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(jLabel9)
                                        .addComponent(jDateChooserin, javax.swing.GroupLayout.DEFAULT_SIZE, 138, Short.MAX_VALUE)
                                        .addComponent(jLabel10)
                                        .addComponent(jDateChooserout, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addComponent(jLabel12)
                                    .addComponent(jLabel11)
                                    .addComponent(jComboBox3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel13)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(10, 10, 10)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(noM)
                                            .addComponent(siM))))
                                .addGap(143, 143, 143)
                                .addComponent(jLabel14))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel15)
                                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 221, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(71, 71, 71)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 211, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabelCantHabs))))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(jLabel18)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel17)
                    .addComponent(jLabel18))
                .addGap(24, 24, 24)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(jLabel14))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(jLabel10))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(Sicn)
                            .addComponent(jDateChooserin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(nocn)
                            .addComponent(jLabel11))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jDateChooserout, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jComboBoxcliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel12))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jComboBox3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(14, 14, 14)
                        .addComponent(jLabel3)
                        .addGap(5, 5, 5)
                        .addComponent(jLabel13)
                        .addGap(6, 6, 6)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(siM))
                        .addGap(4, 4, 4)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTextFieldnom, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(noM))
                        .addGap(9, 9, 9)
                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jTextFieldape, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(1, 1, 1)
                                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(4, 4, 4)
                                .addComponent(jTextFielddoc, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextFielddom, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jComboBoxproce, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel15)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 357, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabelCantHabs)))
                .addGap(95, 95, 95)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jButton2))
                .addGap(35, 35, 35))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 903, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 746, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jLabel18MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel18MouseClicked
        Main m = new Main();
        this.setVisible(false);
        m.setVisible(true);
    }//GEN-LAST:event_jLabel18MouseClicked

    private void jLabel17MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel17MouseClicked
        // TODO add your handling code here:
        //System.exit(0);
        this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
    }//GEN-LAST:event_jLabel17MouseClicked

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        try {
            Reservar();
        } catch (SQLException ex) {
            Logger.getLogger(ReservarHabitacion.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ReservarHabitacion.class.getName()).log(Level.SEVERE, null, ex);
        }
        limpiarTabla();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton2MouseClicked

    }//GEN-LAST:event_jButton2MouseClicked

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        nocn.setSelected(true);
        jComboBoxcliente.setSelectedIndex(0);
        jTextFieldnom.setText("");
        jTextFieldape.setText("");
        jTextFielddoc.setText("");
        jTextFielddom.setText("");
        jComboBoxproce.setSelectedIndex(0);
        jDateChooserin.setCalendar(null);
        jDateChooserout.setCalendar(null);
        jTextArea1.setText("");
        jComboBox3.setSelectedIndex(0);
        noM.setSelected(true);
        try {
            CargarTabla();
        } catch (SQLException ex) {
            Logger.getLogger(ReservarHabitacion.class.getName()).log(Level.SEVERE, null, ex);
        }
        jComboBoxcliente.setEnabled(true);
        jTextFieldape.setEnabled(false);
        jTextFieldnom.setEnabled(false);
        jTextFielddom.setEnabled(false);
        jTextFielddoc.setEnabled(false);
        jComboBoxproce.setEnabled(false);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void noMActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_noMActionPerformed
        Date i,o;
        i=jDateChooserin.getDate();
        o=jDateChooserout.getDate();
        filtrarFecha2(i, o);
    }//GEN-LAST:event_noMActionPerformed

    private void siMActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_siMActionPerformed
        Date i,o;
        i=jDateChooserin.getDate();
        o=jDateChooserout.getDate();
        filtrarFecha2(i, o);
    }//GEN-LAST:event_siMActionPerformed

    private void jComboBox3PropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jComboBox3PropertyChange

    }//GEN-LAST:event_jComboBox3PropertyChange

    private void jComboBox3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox3ActionPerformed

        Date i,o;
        i=jDateChooserin.getDate();
        o=jDateChooserout.getDate();
        filtrarFecha2(i, o);
        /*        // TODO add your handling code here:
        if(jComboBox3.getSelectedItem() == "SIMPLE" && noM.isSelected()){
            try {
                MostrarSimplesnM();
            } catch (SQLException ex) {
                Logger.getLogger(ReservarHabitacion.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else if(jComboBox3.getSelectedItem() == "SIMPLE" && siM.isSelected())
        limpiarTabla();
        if(jComboBox3.getSelectedItem() == "DOBLE" && noM.isSelected()){
            try {
                MostrarDoblesnM();
            } catch (SQLException ex) {
                Logger.getLogger(ReservarHabitacion.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else if (jComboBox3.getSelectedItem() == "DOBLE" && siM.isSelected()){
            try {
                MostrarDoblesM();
            } catch (SQLException ex) {
                Logger.getLogger(ReservarHabitacion.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if(jComboBox3.getSelectedItem() == "TRIPLE" && noM.isSelected()){
            try {
                MostrarTriplesnM();
            } catch (SQLException ex) {
                Logger.getLogger(ReservarHabitacion.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else if (jComboBox3.getSelectedItem() == "TRIPLE" && siM.isSelected()){
            try {
                MostrarTriplesM();
            } catch (SQLException ex) {
                Logger.getLogger(ReservarHabitacion.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if(jComboBox3.getSelectedItem() == "CUADRUPLE" && noM.isSelected()){
            try {
                MostrarCuadruplesnM();
            } catch (SQLException ex) {
                Logger.getLogger(ReservarHabitacion.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else if (jComboBox3.getSelectedItem() == "CUADRUPLE" && siM.isSelected()){
            try {
                MostrarCuadruplesM();
            } catch (SQLException ex) {
                Logger.getLogger(ReservarHabitacion.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if(jComboBox3.getSelectedItem() == "QUINTUPLE" && noM.isSelected()){
            try {
                MostrarQuintuplesnM();
            } catch (SQLException ex) {
                Logger.getLogger(ReservarHabitacion.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else if (jComboBox3.getSelectedItem() == "QUINTUPLE" && siM.isSelected()){
            try {
                MostrarQuintuplesM();
            } catch (SQLException ex) {
                Logger.getLogger(ReservarHabitacion.class.getName()).log(Level.SEVERE, null, ex);
            }
        }*/
    }//GEN-LAST:event_jComboBox3ActionPerformed

    private void jDateChooseroutPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jDateChooseroutPropertyChange
        Date i;
        Date o;
        i = jDateChooserin.getDate();
        o = jDateChooserout.getDate();
        filtrarFecha2(i, o);
        /*int hab;
        ResultSet rs = mr.getReservas();
        RIterator ri = new RIterator(rs);
        ResultSet rh = mh.getHabitaciones();
        HIterator habis = new HIterator(rh);
        Object [] habs = (Object[]) habis.next();
        int tipo = 0;
        limpiarTabla();
        if (i != null && o != null){
            Object [] res = (Object[]) ri.next(i, o);
            if(res !=null)
            hab = (int) res [8];
            Object [] fila = null;
            int h = mr.getRowCount();
            for (int j = 0;j<h;j++){
                if (res == null){
                    res = (Object[]) ri.next(i, o);
                }
                else {
                    hab = (int) res [8];
                    while (habis.hasNext()){
                        int num = (int) habs[4];
                        if(num == hab){
                            tipo = (int) habs[1];
                        }

                    }
                    fila[0]=hab;
                    if(tipo == 1){
                        fila[1] = "SIMPLE";
                    }
                    if(tipo == 2){
                        fila[1] = "DOBLE";
                    }
                    if(tipo == 3){
                        fila[1] = "TRIPLE";
                    }
                    if(tipo == 4){
                        fila[1] = "CUADRUPLE";
                    }
                    if(tipo == 5){
                        fila[1] = "QUINTUPLE";
                    }
                    dtm0.addRow(fila);
                    //dtm0.addRow(res);
                    res = (Object[]) ri.next(i, o);
                }
            }
        }*/
    }//GEN-LAST:event_jDateChooseroutPropertyChange

    private void jDateChooserinPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jDateChooserinPropertyChange
        Date d;
        d = jDateChooserin.getDate();
        jDateChooserout.setMinSelectableDate(d);

    }//GEN-LAST:event_jDateChooserinPropertyChange

    private void jTextFielddomKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFielddomKeyTyped
        char c=evt.getKeyChar();
        if(Character.isDigit(c)) {
            getToolkit().beep();
            evt.consume();
        }
    }//GEN-LAST:event_jTextFielddomKeyTyped

    private void jTextFielddocKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFielddocKeyTyped
        char c=evt.getKeyChar();
        if(Character.isLetter(c)) {
            getToolkit().beep();
            evt.consume();
        }
    }//GEN-LAST:event_jTextFielddocKeyTyped

    private void jTextFieldapeKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldapeKeyTyped
        char c=evt.getKeyChar();
        if(Character.isDigit(c)) {
            getToolkit().beep();
            evt.consume();
        }
    }//GEN-LAST:event_jTextFieldapeKeyTyped

    private void jTextFieldnomKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldnomKeyTyped
        // TODO add your handling code here:
        char c=evt.getKeyChar();
        if(Character.isDigit(c)) {
            getToolkit().beep();
            evt.consume();
        }
    }//GEN-LAST:event_jTextFieldnomKeyTyped

    private void jTextFieldnomActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldnomActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldnomActionPerformed

    private void jComboBoxclienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxclienteActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboBoxclienteActionPerformed

    private void nocnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nocnActionPerformed
        jComboBoxcliente.setEnabled(true);
        jTextFieldape.setEnabled(false);
        jTextFieldnom.setEnabled(false);
        jTextFielddom.setEnabled(false);
        jTextFielddoc.setEnabled(false);
        jComboBoxproce.setEnabled(false);
    }//GEN-LAST:event_nocnActionPerformed

    private void SicnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SicnActionPerformed
        jComboBoxcliente.setEnabled(false);
        //jComboBoxcliente.setEnabled(true);
        jTextFieldape.setEnabled(true);
        jTextFieldnom.setEnabled(true);
        jTextFielddom.setEnabled(true);
        jTextFielddoc.setEnabled(true);
        jComboBoxproce.setEnabled(true);
    }//GEN-LAST:event_SicnActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ReservarHabitacion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ReservarHabitacion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ReservarHabitacion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ReservarHabitacion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new ReservarHabitacion().setVisible(true);
                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JRadioButton Sicn;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JComboBox<String> jComboBox3;
    private javax.swing.JComboBox<String> jComboBoxcliente;
    private javax.swing.JComboBox<String> jComboBoxproce;
    private com.toedter.calendar.JDateChooser jDateChooserin;
    private com.toedter.calendar.JDateChooser jDateChooserout;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLabelCantHabs;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JTextField jTextFieldape;
    private javax.swing.JTextField jTextFielddoc;
    private javax.swing.JTextField jTextFielddom;
    private javax.swing.JTextField jTextFieldnom;
    private javax.swing.ButtonGroup matrimonial;
    private javax.swing.ButtonGroup newClient;
    private javax.swing.JRadioButton noM;
    private javax.swing.JRadioButton nocn;
    private javax.swing.JRadioButton siM;
    // End of variables declaration//GEN-END:variables
}
