/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import hotelisii.ManagerHabitacion;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import static javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Mauro
 */
public class ListarHabitacion extends javax.swing.JFrame {
    private void CerrarVentana(){
          addWindowListener(new WindowAdapter() {
             @Override
             public void windowClosing(WindowEvent e) {
                int confirm = JOptionPane.showOptionDialog(
                null, "¿Está seguro que desea salir? ", 
                 "Salir", JOptionPane.YES_NO_OPTION,  
                JOptionPane.QUESTION_MESSAGE, null, null, null);
                if (confirm == 0) {
                System.exit(0);
                }
                else{
                    setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
                }
             }
          });
        
    }
    /**
     * Creates new form ListarHabitacion
     */
    //DefaultTableModel dt = new DefaultTableModel();
    ManagerHabitacion mh = new ManagerHabitacion();
    //ArrayList<Habitacion> listHab = new ArrayList<>();
    
    public ListarHabitacion() throws SQLException {
        initComponents();
        CerrarVentana();
        this.setLocationRelativeTo(null);
        try{
            mostrarHabitacion();
        }catch(SQLException e){
            
        }
        
    }

    
    public void mostrarHabitacion() throws SQLException{
        DefaultTableModel model = (DefaultTableModel)listadoHabitaciones.getModel();
        ResultSet rs = mh.getHabitaciones();
        Object[] row = new Object[4];
        try{
            while(rs.next())
            {
                row[0] = rs.getInt("H_NROH");
                row[1] = rs.getInt("H_ESTADO");
                if(rs.getInt("H_TIPO") == 1){
                    row[2] = "SIMPLE";
                }
                if(rs.getInt("H_TIPO") == 2){
                    row[2] = "DOBLE";
                }
                if(rs.getInt("H_TIPO") == 3){
                    row[2] = "TRIPLE";
                }
                if(rs.getInt("H_TIPO") == 4){
                    row[2] = "CUADRUPLE";
                }
                if(rs.getInt("H_TIPO") == 5){
                    row[2] = "QUINTUPLE";
                }
                row[3] = rs.getFloat("H_PRECIO");
                model.addRow(row);
                //listadoHabitaciones.setModel(model);
            }
        }catch(SQLException e){
  
        }
    }
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        listadoHabitaciones = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(32, 53, 86));
        setLocation(new java.awt.Point(700, 700));
        setLocationByPlatform(true);
        setUndecorated(true);
        setSize(new java.awt.Dimension(10, 1));

        jPanel1.setBackground(new java.awt.Color(32, 53, 86));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("sansserif", 0, 30)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(153, 0, 0));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("X");
        jLabel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel1MouseClicked(evt);
            }
        });
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(1250, 0, 30, 40));

        jLabel2.setFont(new java.awt.Font("sansserif", 0, 36)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("         Listado de habitaciones ");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 80, 581, 52));

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/GUI/icons8-volver-30.png"))); // NOI18N
        jLabel4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel4MouseClicked(evt);
            }
        });
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));

        listadoHabitaciones.setFont(new java.awt.Font("sansserif", 0, 17)); // NOI18N
        listadoHabitaciones.setForeground(new java.awt.Color(51, 51, 51));
        listadoHabitaciones.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Numero Habitacion", "Estado", "Tipo", "Precio"
            }
        ));
        listadoHabitaciones.setToolTipText("");
        listadoHabitaciones.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_ALL_COLUMNS);
        listadoHabitaciones.setGridColor(new java.awt.Color(255, 255, 255));
        listadoHabitaciones.setRowHeight(20);
        listadoHabitaciones.setSelectionBackground(new java.awt.Color(32, 53, 86));
        jScrollPane1.setViewportView(listadoHabitaciones);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 219, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 489, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jLabel1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel1MouseClicked
        // TODO add your handling code here:
        this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
    }//GEN-LAST:event_jLabel1MouseClicked

    private void jLabel4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel4MouseClicked
        Main m = new Main();
        this.setVisible(false);
        m.setVisible(true);
    }//GEN-LAST:event_jLabel4MouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ListarHabitacion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ListarHabitacion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ListarHabitacion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ListarHabitacion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new ListarHabitacion().setVisible(true);
                } catch (SQLException ex) {
                    Logger.getLogger(ListarHabitacion.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable listadoHabitaciones;
    // End of variables declaration//GEN-END:variables
}
